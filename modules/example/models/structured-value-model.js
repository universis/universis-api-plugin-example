import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('StructuredValue')
class StructuredValue extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = StructuredValue;